from flask import Flask
import dateutil.utils
import discord
import requests
import json
import pandas as pd
from discord.ext import commands
from discord.ext import tasks
import datetime
import time
import myDatabase
import os

intents = discord.Intents.all()
intents.members = True
bot = commands.Bot(command_prefix="!", intents=intents)
discord_token = os.getenv('DISCORDTOKEN')
client = discord.Client(intents=intents)
app = Flask(__name__)

def formatted_print(obj):
    text = json.dumps(obj, sort_keys=True, indent=4)
    return text


def handle_user_messages(msg) -> str:
    message = msg.lower()  # Converts all inputs to lower case
    print('handles: ' + msg)
    filename = ""
    resultdata=""

    today=dateutil.utils.today()
    todayunix=time.mktime(today.timetuple())
    yesterday = dateutil.utils.today() - datetime.timedelta(days = 1)
    yesterdayunix=time.mktime(yesterday.timetuple())
    thirtydays = dateutil.utils.today() - datetime.timedelta(days = 30)
    thirtydaysunix = time.mktime(thirtydays.timetuple())
    query={}

    if message.__contains__('today'):
        query = {"time": {"$gte": yesterdayunix}}
    elif message.__contains__('30'):
        query = {"time": {"$gte": thirtydaysunix}}
    if message.startswith('!mavenlog'):
        resultdata = myDatabase.read_from_db('mongoclient_maven','Maven','Maven Stash', query)
        filename = 'mavenlog.csv'
    elif message.startswith('!envoylog'):
        resultdata = myDatabase.read_from_db('mongoclient_envoy','Envoy', 'Envoy Stash', query)
        filename = 'envoylog.csv'
    elif message.startswith('!exarchlog'):
        resultdata = myDatabase.read_from_db('mongoclient','Exarch', 'Exarch Stash', query)
        filename = 'exarchlog.csv'

    getDataIntoTable(resultdata,filename)
    return filename


def getDataIntoTable(message, filename) -> str:
    myjson = json.loads(message)
    temp_df = pd.json_normalize(myjson)
    temp_df['time'] = pd.to_datetime(temp_df['time'], unit='s')
    cols = ['time', 'league', 'account.name', 'action', 'stash', 'item']
    temp_df = temp_df[cols]
    temp_df = temp_df.rename(columns={'time': 'Date', 'account.name': 'Account', 'league': 'League', 'action': 'Action', 'stash': 'Stash', 'item': 'Item'})
    temp_df.to_csv(filename,index=False)


async def processMessage(message):
    try:
        botfeedback = handle_user_messages(message.content)
        if botfeedback != "":
            #await message.channel.send(botfeedback)
            await message.channel.send(file=discord.File(botfeedback))
    except Exception as error:
        print(error)


@bot.event
async def on_message(message):
    print(f'USER - {message.author} texted - {message.content}')
    await processMessage(message)


@app.route('/')
def hello_world():
    return 'Hello, World!'


print("started")
bot.run(discord_token)

