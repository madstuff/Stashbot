import json

from pymongo import MongoClient
from bson.json_util import dumps
import os


def read_from_db(connection, clientDB, collectionIn, myquery) -> str:

  client = MongoClient(os.getenv(connection))
  db = client[clientDB]
  collection = db[collectionIn]
  mydoc = collection.find(myquery)
  texterg=dumps(mydoc)
  client.close()
  return texterg


#print(read_exarch())